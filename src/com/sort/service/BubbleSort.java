package com.sort.service;

import java.util.Scanner;

public class BubbleSort {

	// structure of a node
	static class Node {
		int data;
		Node prev;
		Node next;
	};

	// Function to insert a node at the beginning of a linked list
	static Node insertAtTheBegin(Node start_ref, int data) {
		Node ptr1 = new Node();
		ptr1.data = data;
		ptr1.next = start_ref;
		if (start_ref != null)
			(start_ref).prev = ptr1;
		start_ref = ptr1;
		return start_ref;
	}

	// Function to print nodes in a given linked list
	static void printList(Node start) {
		Node temp = start;
		System.out.println();
		while (temp != null) {
			System.out.print(temp.data + " ");
			temp = temp.next;
		}
	}

	// Bubble sort the given linked list
	static Node bubbleSort(Node start) {
		@SuppressWarnings("unused")
		int swapped, i;
		Node ptr1;
		Node lptr = null;

		// Checking for empty list
		if (start == null)
			return null;

		do {
			swapped = 0;
			ptr1 = start;

			while (ptr1.next != lptr) {
				if (ptr1.data > ptr1.next.data) {
					int t = ptr1.data;
					ptr1.data = ptr1.next.data;
					ptr1.next.data = t;
					swapped = 1;
				}
				ptr1 = ptr1.next;
			}
			lptr = ptr1;
		} while (swapped != 0);
		return start;
	}

	// Driver code
	public static void main(String args[]) {

		// start with empty linked list
		Node start = null;

		@SuppressWarnings("unused")
		int list_size, i;

		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);

		int select;
		int term = 0;
		while (term == 0) {
			System.out.print(
					"\nOption:\tTo Do:\n1\tTo Add element.\n2\tTo Display element.\n3\t Bubble Sort.\n4\tTo Exit From Program.\nEnter your option:- ");
			select = scan.nextInt();
			switch (select) {
			case 1: {
				System.out.print("Enter element to insert:- ");
				int value = scan.nextInt();
				start = insertAtTheBegin(start, value);
				break;
			}
			case 2: {
				System.out.print("Elements in list are- ");
				printList(start);
				break;
			}
			case 3: {
				System.out.print("Bubble Sort ");
				start = bubbleSort(start);
				break;
			}

			case 4: {
				term = 1;
				System.out.println("Thank you!");
				break;
			}
			default:
				System.out.println("Enter a valid options");
			}
		}
	}

}
