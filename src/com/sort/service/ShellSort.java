package com.sort.service;

import java.util.Scanner;

public class ShellSort {

	// Shell Sort
	static void shellsort(int[] arr, int num) {
		int i, j, k, tmp;
		for (i = num / 2; i > 0; i = i / 2) {
			for (j = i; j < num; j++) {
				for (k = j - i; k >= 0; k = k - i) {
					if (arr[k + i] >= arr[k])
						break;
					else {
						tmp = arr[k];
						arr[k] = arr[k + i];
						arr[k + i] = tmp;
					}
				}
			}
		}
	}

	// Merge Sort
	public static void merge(int[] left_arr, int[] right_arr, int[] arr, int left_size, int right_size) {

		int i = 0, l = 0, r = 0;
		// The while loops check the conditions for merging
		while (l < left_size && r < right_size) {

			if (left_arr[l] < right_arr[r]) {
				arr[i++] = left_arr[l++];
			} else {
				arr[i++] = right_arr[r++];
			}
		}
		while (l < left_size) {
			arr[i++] = left_arr[l++];
		}
		while (r < right_size) {
			arr[i++] = right_arr[r++];
		}
	}

	// Merge Sort
	public static void mergeSort(int[] arr, int len) {
		if (len < 2) {
			return;
		}

		int mid = len / 2;
		int[] left_arr = new int[mid];
		int[] right_arr = new int[len - mid];

		// Dividing array into two and copying into two separate arrays
		int k = 0;
		for (int i = 0; i < len; ++i) {
			if (i < mid) {
				left_arr[i] = arr[i];
			} else {
				right_arr[k] = arr[i];
				k = k + 1;
			}
		}
		// Recursively calling the function to divide the subarrays further
		mergeSort(left_arr, mid);
		mergeSort(right_arr, len - mid);
		// Calling the merge method on each subdivision
		merge(left_arr, right_arr, arr, mid, len - mid);
	}
	
	

	public static void main(String[] args) {
		ShellSort sort = new ShellSort();

		// Shell Sort
		Scanner sc = new Scanner(System.in);
		int arr[] = new int[30];
		int k, num;
		
		int n = arr.length; 

		System.out.println("Enter total no. of elements : ");
		num = sc.nextInt();

		int select;
		int term = 0;
		while (term == 0) {
			System.out.print("\nOption:\tTo Do:\n1\tTo Add element.\n2\tShell Sort.\n3\tTo view Sorted List."
					+ "\n4\tMerge Sort.\n5\tTo Exit From Program.\nEnter your option:- ");
			select = sc.nextInt();
			switch (select) {
			case 1: {
				System.out.print("Enter element to insert:- ");

				for (k = 0; k < num; k++) {
					arr[k] = sc.nextInt();
				}
				break;
			}
			case 2: {
				System.out.println("\n Shell Sort is perfomed :");
				shellsort(arr, num);
				break;
			}
			case 3: {
				System.out.println("\n  Sorted List : ");
				for (k = 0; k < num; k++)
					System.out.println(arr[k]);
				break;
			}
			case 4: {
				System.out.println("\nMerge Sort is perfomed :");
				mergeSort(arr, num);
				break;

			}
		

			case 5: {
				term = 1;
				System.out.println("Thank you!");
				break;
			}
			default:
				System.out.println("Enter a valid options");
			}
		}
	}

}
